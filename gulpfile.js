/**
 * Config.
 */

// Project vars.
var project                 = 'DaxMaterialize'; // Project name.
var projectURL              = 'http://localhost/wordpress/'; // Local env path.
var productURL              = './';

// Styles.
var styleSRC                = './src/sass/dax_materialize.scss'; // Main SASS file.
var styleDestination        = './css/'; // Compiled CSS file path.

// JavaScript.
var jsCustomSRC             = './src/js/*.js'; // Custom js file path.
var jsCustomDestination     = './js/'; // Compiled js file path.
var jsCustomFile            = 'tactic_center_child'; // Compiled js filename.

// Rutas de archivos a chequear cuando se realicen cambios.
var styleWatchFiles         = './src/sass/*.scss'; // Path to SCSS files to watch.
var customJSWatchFiles      = './src/js/*.js'; // Path to jS files to watch.
var projectPHPWatchFiles    = './**/*.php'; // Path to PHP files to watch.

// Browserlist https://github.com/ai/browserslist
const AUTOPREFIXER_BROWSERS = [
    'last 2 version',
    '> 1%',
    'ie >= 9',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4',
    'bb >= 10'
  ];
/**************** Ends main env variables *********************/

var gulp         = require('gulp');
var sass         = require('gulp-sass');
var minifycss    = require('gulp-uglifycss');
var autoprefixer = require('gulp-autoprefixer');
var mmq          = require('gulp-merge-media-queries');
var concat       = require('gulp-concat');
var uglify       = require('gulp-uglify');
var rename       = require('gulp-rename');
var lineec       = require('gulp-line-ending-corrector');
var filter       = require('gulp-filter');
var sourcemaps   = require('gulp-sourcemaps');
var notify       = require('gulp-notify');
var browserSync  = require('browser-sync').create();
var reload       = browserSync.reload;

/**
 * Task: 'browser-sync'
 * Refresh the browser if changes are made.
 */
gulp.task( 'browser-sync', function() {
  browserSync.init( {

    proxy: projectURL,
    open: true,
    injectChanges: true,

    // Use specific port.
    // port: 7000,

  } );
});

/**
 * Task: 'styles'.
 * Compiles Sass.
 */
 gulp.task('styles', function () {
    gulp.src( styleSRC )
    .pipe( sourcemaps.init() )
    .pipe( sass( {
      errLogToConsole: true,
      outputStyle: 'compact',
      precision: 10
    } ) )
    .on('error', console.error.bind(console))
    .pipe( sourcemaps.write( { includeContent: false } ) )
    .pipe( sourcemaps.init( { loadMaps: true } ) )
    .pipe( autoprefixer( AUTOPREFIXER_BROWSERS ) )

    .pipe( sourcemaps.write ( './' ) )
    .pipe( lineec() )
    .pipe( gulp.dest( styleDestination ) )

    .pipe( filter( '**/*.css' ) )
    .pipe( mmq( { log: true } ) )

    .pipe( browserSync.stream() )
 });

 /**
  * Task: 'stylesmin'.
  * Compiles Sass and creates a minified CSS file.
  */
  gulp.task('stylesmin', function () {
     gulp.src( styleSRC )
     .pipe( sourcemaps.init() )
     .pipe( sass( {
       errLogToConsole: true,
       // outputStyle: 'compact',
       outputStyle: 'compressed',
       // outputStyle: 'nested',
       // outputStyle: 'expanded',
       precision: 10
     } ) )
     .on('error', console.error.bind(console))
     .pipe( sourcemaps.write( { includeContent: false } ) )
     .pipe( sourcemaps.init( { loadMaps: true } ) )
     .pipe( autoprefixer( AUTOPREFIXER_BROWSERS ) )

     .pipe( sourcemaps.write ( './' ) )
     .pipe( lineec() )
     .pipe( gulp.dest( styleDestination ) )

     .pipe( filter( '**/*.css' ) )
     .pipe( mmq( { log: true } ) )

     .pipe( browserSync.stream() )

     .pipe( rename( { suffix: '.min' } ) )
     .pipe( minifycss( {
       maxLineLen: 0
     }))
     .pipe( lineec() )
     .pipe( gulp.dest( styleDestination ) )

     .pipe( filter( '**/*.css' ) )
     .pipe( browserSync.stream() )
     .pipe( notify( { message: 'TASK: "stylesmin" Completed! 💯', onLast: true } ) )
  });


  /**
  * Task: `scripts`.
  * Compiles js files and creates a minified version.
  */
 gulp.task( 'scripts', function() {
    gulp.src( jsCustomSRC )
    .pipe( concat( jsCustomFile + '.js' ) )
    .pipe( lineec() )
    .pipe( gulp.dest( jsCustomDestination ) )
    .pipe( rename( {
      basename: jsCustomFile,
      suffix: '.min'
    }))
    .pipe( uglify() )
    .pipe( lineec() )
    .pipe( gulp.dest( jsCustomDestination ) )
    .pipe( notify( { message: 'TASK: "customJs" Completed! 💯', onLast: true } ) );
 });


 /**
  * Default gulp task.
  * Detects changes made and refresh browser.
  */
 gulp.task( 'default', ['styles', 'stylesmin', 'scripts', 'browser-sync'], function () {
  gulp.watch( projectPHPWatchFiles, reload );
  gulp.watch( styleWatchFiles, [ 'styles' ] );
  gulp.watch( customJSWatchFiles, [ 'scripts', reload ] );
 });
